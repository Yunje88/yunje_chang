package com.revature.yunje.tests;

import com.revature.yunje.BankApplication;
import com.revature.yunje.CustomerScreen;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ScreenTests {
    private BankApplication app;

    @Before
    public void init() {
        app = new BankApplication();
        app.init();
    }

    @Test
    public void setsCustomerScreen() {
        CustomerScreen cs = new CustomerScreen();
        String ret = cs.setScreen(app);

        Assert.assertEquals("Incorrect return.", "Customer", ret);
    }
}
