package com.revature.yunje.tests;


import com.revature.yunje.Customer;
import com.revature.yunje.Employee;
import com.revature.yunje.User;
import com.revature.yunje.BankApplication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

class BankTests {
    private BankApplication app;

    @Before
    public void init() {
        app = new BankApplication();
    }

    @Test
    public void setsUser() {
        User user = new Customer();
        app.setCurrentUser(user);

        Assert.assertEquals("User was not set.", user, app.getCurrentUser());
    }

    @Test
    public void getsUser() {
        User user = new Employee();
        app.setCurrentUser(user);

        Assert.assertEquals("Did not get user.", user, app.getCurrentUser());
    }
}
