package com.revature.yunje;

public abstract class User {
    protected boolean isEmployee;
    private double balance;
    private int creditScore;
    private String firstName;
    private String lastName;
    private String userId;
    private String userPw;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(int creditScore) {
        this.creditScore = creditScore;
    }

    public boolean getIsEmployee() {
        return isEmployee;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String email) {
        this.userId = email;
    }

    public String getUserPw() {
        return userPw;
    }

    public void setUserPw(String password) {
        this.userPw = password;
    }

    /**
     * set the user data to Serialized
     * @return serialized
     */
    public String getSerialized() {
        // Serialized Form 1:AccountType 2:AccountId 3:AccountPw 4:FirstName 5:LastName 6:Balance 7:creditScore;
        String serialized = isEmployee + "," + userId + "," + userPw + "," + firstName + "," + lastName + "," + balance + "," + creditScore;
        return  serialized;
    }
}
