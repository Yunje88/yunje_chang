package com.revature.yunje;

import java.util.InputMismatchException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CustomerScreen implements Screen {
    private Logger logger = LogManager.getLogger(CustomerScreen.class.getName());

    /**
     * Set screen for customerScreen
     * @param app
     * @return next page
     */
    @Override
    public String setScreen(AbstractBankApplication app) {
        logger.info("Customer page on Bank Application");

        int option;
        while (true) {
            System.out.println("-------------------------------------------------");
            System.out.println("Hello, " + app.getCurrentUser().getFirstName() + " " + app.getCurrentUser().getLastName() + ".");
            System.out.println("Please select option below.");
            System.out.println("1. Deposit Money to checking Account.");
            System.out.println("2. Withdraw Money From checking Account.");
            System.out.println("3. View Currently Balance.");
            System.out.println("4. View Customer Credit Score.");
            System.out.println("5. Logout.");

            try {
                option = app.getScanner().nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid option. Please Try again");
                app.getScanner().nextLine();
                continue;
            }

            if (option >= 8 || option <= 0) {
                System.out.println("Invalid option. Please Try again");
                app.getScanner().nextLine();
                continue;
            }

            app.getScanner().nextLine();
            break;
        }

        switch (option) {

            case 1:
                return depositMoney(app);
            case 2:
                return withdrawMoney(app);
            case 3:
                return viewBalance(app);
            case 4:
                return viewCreditScore(app);
            case 5:
            default:
                return logout(app);
        }
    }
    /**
     * view customer balance service
     * @param app
     * @return customer screen
     */
    private String viewBalance(AbstractBankApplication app) {
        System.out.println("Customer " + app.getCurrentUser().getFirstName() + " " + app.getCurrentUser().getLastName() + " has " +
                app.getCurrentUser().getBalance() + " Balance on the Checking Account.");
        return "Customer";
    }
    /**
     * view customer deposit service
     * @param app
     * @return customer screen
     */
    private String depositMoney(AbstractBankApplication app) {
        User user = new Customer();

        System.out.println("-------------------------------------------------");
        System.out.println("Customer " + app.getCurrentUser().getFirstName() + " " + app.getCurrentUser().getLastName() + " has " +
                app.getCurrentUser().getBalance() + " Balance on the Checking Account.");
        System.out.print("How much money do you want to deposit on the Checking Account?");
        double depostMoney = app.getScanner().nextDouble();
        double currentMoney = app.getCurrentUser().getBalance();
        double total = depostMoney + currentMoney;
        System.out.println("Now you have " + total + "Amount in your Cheking account");
        user.setBalance(total);

        return "Customer";
    }
    /**
     * view customer withdraw service
     * @param app
     * @return customer screen
     */
    private String withdrawMoney(AbstractBankApplication app) {
        User user = new Customer();

        System.out.println("-------------------------------------------------");
        System.out.println("Customer " + app.getCurrentUser().getFirstName() + " " + app.getCurrentUser().getLastName() + " has " +
                app.getCurrentUser().getBalance() + " Balance on the Checking Account.");
        System.out.print("How much money do you want to withdraw from the Checking Account?");
        double depostMoney = app.getScanner().nextDouble();
        double currentMoney = app.getCurrentUser().getBalance();
        double total = depostMoney - currentMoney;
        System.out.println("Now you have " + total + "Amount in your Cheking account");
        user.setBalance(total);

        return "Customer";
    }
    /**
     * view customer credit score service
     * @param app
     * @return customer screen
     */
    private String viewCreditScore(AbstractBankApplication app) {
        System.out.println("-------------------------------------------------");
        System.out.println("Customer " + app.getCurrentUser().getFirstName() + " " + app.getCurrentUser().getLastName() + " has " + app.getCurrentUser().getCreditScore() + " credit Score.");
        int credit = app.getCurrentUser().getCreditScore();
        if(credit >= 750){
            System.out.println("You have Excellent credit score. You have pre-approval low interest.");
        }else if(credit >= 600){
            System.out.println("You have Good credit score. You have pre-approval low-high interest.");
        }else if(credit >= 500){
            System.out.println("You have Poor credit score. You have to requires credit review.");
        }else{
            System.out.println("You have not enough credit score to apply credit card.");
        }
        return "Customer";
    }

    /**
     * log out current page
     * @param app
     * @return home screen
     */
    private String logout(AbstractBankApplication app) {
        app.setCurrentUser(null);
        System.out.println("Successfully logged out.");
        return "Home";
    }
}
