package com.revature.yunje;

import com.revature.yunje.AbstractBankApplication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HomeScreen implements Screen {
    Logger logger = LogManager.getLogger(HomeScreen.class.getName());

    /**
     * Main screen for bank application
     * @param app
     * @return login screen
     */
    @Override
    public String setScreen(AbstractBankApplication app) {
        logger.info("Main page on Bank Application");
        System.out.println("-------------------------------------------------");
        System.out.println("To Our valued Customer, Welcome to Revature Bank.");
        return "Login";
    }
}
