package com.revature.yunje;

import com.revature.yunje.User;

import java.util.List;

public interface Service {
    public List getAllData();
    public void saveUser(User user);
}
