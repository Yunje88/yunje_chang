package com.revature.yunje;

import com.revature.yunje.Service;

import java.util.List;

public class DataController {
    private Service dataService;

    public DataController() {}
    public DataController(Service dataService) {
        dataService = dataService;
    }

    /**
     * Get user data from the database.
     * @return All user data
     */
    public List getAllData() {
        return dataService.getAllData();
    }
}
