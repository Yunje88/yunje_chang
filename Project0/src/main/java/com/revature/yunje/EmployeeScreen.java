package com.revature.yunje;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.InputMismatchException;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmployeeScreen implements Screen {
    private Logger logger = LogManager.getLogger(EmployeeScreen.class.getName());
    /**
     * Set screen for employeeScreen
     * @param app
     * @return next page
     */
    @Override
    public String setScreen(AbstractBankApplication app) {
        logger.info("Employee page on Bank Application");
        int option;

        while (true) {
            System.out.println("-------------------------------------------------");
            System.out.println("Hello, " + app.getCurrentUser().getFirstName() + " " + app.getCurrentUser().getLastName() + ".");
            System.out.println("Please select one of three option below.");
            System.out.println("1. Create a new customer or employee account.");
            System.out.println("2. View the customer credit score.");
            System.out.println("3. Logout");

            try {
                option = app.getScanner().nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid option. Please Try again");
                app.getScanner().nextLine();
                continue;
            }

            if (option >= 4 || option <= 0) {
                System.out.println("Invalid option. Please Try again");
                app.getScanner().nextLine();
                continue;
            }

            app.getScanner().nextLine();
            break;
        }

        switch (option) {
            case 1:
                return createAccount(app);
            case 2:
                return viewCreditScore(app);
            case 3:
            default:
                return logout(app);
        }
    }
    /**
     * logout current page
     * @param app
     * @return home screen
     */
    private String logout(AbstractBankApplication app) {
        app.setCurrentUser(null);
        System.out.println("Successfully logged out.");
        System.out.println("-------------------------------------------------");
        return "Home";
    }
    /**
     * create a user account
     * @param app
     * @return employee screen
     */
    private String createAccount(AbstractBankApplication app) {
        System.out.println("-------------------------------------------------");
        System.out.print("Please enter Account ID here:");
        String accountId = app.getScanner().nextLine();
        System.out.print("Please enter Account Password here:");
        String accountPw = app.getScanner().nextLine();
        System.out.print("Please enter User First name here:");
        String firstName = app.getScanner().nextLine();
        System.out.print("Please enter User Last here:");
        String lastName = app.getScanner().nextLine();
        System.out.print("How much money do you want to deposit:");
        double balance = app.getScanner().nextDouble();
        System.out.println("Please enter the Customer Credit Score here: ");
        int creditScore = app.getScanner().nextInt();
        System.out.println("Choose the Account type here(1 for Customer, 2 for Employee): ");
        System.out.println("1. CUSTOMER");
        System.out.println("2. EMPLOYEE");
        int employeeValue = app.getScanner().nextInt();

        User user;
        if (employeeValue == 2) {
            user = new Employee();
        } else {
            user = new Customer();
        }

        user.setUserId(accountId);
        user.setUserPw(accountPw);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setBalance(balance);
        user.setCreditScore(creditScore);

        Service service = (Service)app.getContext().get("Service");
        service.saveUser(user);

        System.out.println("User \'" + user.getUserId() + "\' created successfully.");

        return "Employee";
    }
    /**
     * view customer credit score.
     * @param app
     * @return next page
     */
    private String viewCreditScore(AbstractBankApplication app){
         System.out.println("Customer " + app.getCurrentUser().getFirstName() + " " + app.getCurrentUser().getLastName() + " has " +
                app.getCurrentUser().getCreditScore() + " credit Score.");
        int credit = app.getCurrentUser().getCreditScore();
        if(credit >= 750){
            System.out.println("You have Excellent credit score. You have pre-approval low interest.");
        }else if(credit >= 600){
            System.out.println("You have Good credit score. You have pre-approval low-high interest.");
        }else if(credit >= 500){
            System.out.println("You have Poor credit score. You have to requires credit review.");
        }else{
            System.out.println("You have not enough credit score to apply credit card.");
        }
        return "Employee";
    }

}
