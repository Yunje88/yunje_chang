package com.revature.yunje;

import com.revature.yunje.DataController;
import com.revature.yunje.Dao;
import com.revature.yunje.FileDao;
import com.revature.yunje.Screen;
import com.revature.yunje.HomeScreen;
import com.revature.yunje.LoginScreen;
import com.revature.yunje.EmployeeScreen;
import com.revature.yunje.DataService;
import com.revature.yunje.Service;
import com.revature.yunje.User;

import java.io.File;
import java.util.HashMap;
import java.util.Scanner;

public class BankApplication extends AbstractBankApplication {
    private Screen currentScreen;
    private User currentUser = null;


    /**
     * Begin application from the home Screen.
     */
    @Override
    public void run() {
        init();

        currentScreen = ((HashMap<String, Screen>)context.get("Screens")).get("Home");

        while (currentScreen != null) {
            String next = currentScreen.setScreen(this);
            currentScreen = ((HashMap<String, Screen>) context.get("Screens")).get(next);
        }
    }

    /**
     * Get the User logged in.
     * @return logged in user.
     */
    @Override
    public User getCurrentUser() {
        return  currentUser;
    }

    /**
     * set the User logged in
     * @param user
     */
    public void setCurrentUser(User user) {
        currentUser = user;
    }

    /**
     * Initialize all functions
     */
    public void init() {
        context = new HashMap<String, Object>();
        scanner = new Scanner(System.in);
        Dao dao = new FileDao();
        Service service = new DataService(dao);
        DataController controller = new DataController(service);
        context.put("Dao", dao);
        context.put("Service", service);
        context.put("DataController", controller);

        initScreens();
    }

    /**
     * Initialized all the screens
     */
    private void initScreens() {
        context.put("Screens", new HashMap<String, Screen>());

        HashMap<String, Screen> screens = (HashMap<String, Screen>)context.get("Screens");
        screens.put("Home", new HomeScreen());
        screens.put("Login", new LoginScreen());
        screens.put("Employee", new EmployeeScreen());
        screens.put("Customer", new CustomerScreen());
    }
}
