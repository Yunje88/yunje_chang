package com.revature.yunje;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    /**
     * main class
     * @param args
     */
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Main.class.getName());
        //Initialize and run the Revature bank application.
        BankApplication app = new BankApplication();
        logger.info("Starting Bank Application");
        app.run();
    }
}

