package com.revature.yunje;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileDao implements Dao {
    private  static  final String FILENAME = "src/main/resources/repository.txt";

    /**
     * save user data from serialized
     * @param serialized
     */
    @Override
    public void saveData(String serialized) {
        FileWriter writer = null;

        try {
            writer = new FileWriter(FILENAME, true);
            writer.write(serialized + "\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException("Could not write data to file.", e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    throw new RuntimeException("Could not close writer.", e);
                }
            }
        }
    }

    @Override
    /**
     * get user data from the file.
     */
    public List getAllData() {
        FileReader reader = null;
        Scanner scanner = null;
        List<User> users = new ArrayList<>();

        try {
            reader = new FileReader(FILENAME);
            scanner = new Scanner(reader);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] tokens = line.split(",");
                int tokenLen = tokens.length;

                User u = null;

                if (tokens[0].equals("true")) {
                    u = new Employee();
                } else {
                    u = new Customer();
                }

                u.setUserId(tokens[1]);
                u.setUserPw(tokens[2]);
                u.setFirstName(tokens[3]);
                u.setLastName(tokens[4]);
                u.setBalance(Double.parseDouble(tokens[5]));
                u.setCreditScore(Integer.parseInt(tokens[6]));
                users.add(u);
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException("Could not find user file.", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new RuntimeException("Could not close reader.", e);
                }
            }

            if (scanner != null) {
                scanner.close();
            }
        }

        return users;
    }
}
