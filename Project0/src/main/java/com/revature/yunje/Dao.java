package com.revature.yunje;

import java.util.List;

public interface Dao {
    /**
     * get all user data.
     * @return all user data in list.
     */
    List getAllData();

    /**
     * save user data for serialized.
     * @param serialized
     */
    void saveData(String serialized);
}

