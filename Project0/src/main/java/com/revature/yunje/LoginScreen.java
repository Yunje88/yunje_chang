package com.revature.yunje;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginScreen implements Screen {
    private Logger logger = LogManager.getLogger(LoginScreen.class.getName());

    /**
     * Account Log in page.
     * @param app
     * @return employee screen or customer screen.
     */
    @Override
    public String setScreen(AbstractBankApplication app) {
        logger.info("Login page on Bank Application");
        Service service = (Service)app.getContext().get("Service");
        System.out.print("Please Enter User ID here:");

        List<User> users = service.getAllData();
        User user = null;
        while (user == null) {
            String userId = app.getScanner().nextLine();

            for (User u : users) {
                if (u.getUserId().equals(userId)) {
                    user = u;
                    break;
                }
            }

            if (user == null) {
                System.out.println("User ID does not exist. Please enter a valid Id again:");
            }
        }

        System.out.print("Please enter User Password here:");

        while (true) {
            String password = app.getScanner().nextLine();

            if (user.getUserPw().equals(password)) {
                break;
            }

            System.out.print("Password does not match User ID. Please try again here: ");
        }

        app.setCurrentUser(user);

        System.out.println("Welcome to the Revature bank " + user.getUserId() + ".");

        if (user.getIsEmployee()) {
            return "Employee";
        }

        return "Customer";
    }
}
