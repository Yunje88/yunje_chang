package com.revature.yunje;

import com.revature.yunje.User;

import java.util.Map;
import java.util.Scanner;

public abstract class AbstractBankApplication {
    protected Map<String, Object> context;
    protected Scanner scanner;

    public abstract void run();
    public abstract User getCurrentUser();
    public abstract void setCurrentUser(User user);

    public Map<String, Object> getContext(){
        return  context;
    }
    public Scanner getScanner() { return  scanner; }
}
