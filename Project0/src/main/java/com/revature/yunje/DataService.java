package com.revature.yunje;

import com.revature.yunje.Dao;
import com.revature.yunje.User;

import java.util.List;

public class DataService implements Service {
    private Dao dao;
    private List<User> users = null;

    public  DataService() {}
    public  DataService(Dao dao) {
        this.dao = dao;
    }

    public List<User> getUsers() {
        return users;
    }

    @Override
    public List getAllData() {
        if (users == null) {
            users = dao.getAllData();
        }

        return  users;
    }

    @Override
    public void saveUser(User user) {
        users.add(user);
        dao.saveData(user.getSerialized());
    }
}
